<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('infopages', 'InformationPageController@index');
Route::get('infopages/{informationPage}', 'InformationPageController@edit');
Route::patch('infopages/{informationPage}', 'InformationPageController@update');
Route::delete('infopages/picremove/{photo}','PhotoController@destroy');

Route::get('infopage/create', 'InformationPageController@create');
Route::post('/infopage/addnew', 'InformationPageController@store');

Route::get('projects', 'ProjectController@index');
Route::get('projects/{project}', 'ProjectController@edit');
Route::patch('projects/{project}', 'ProjectController@update');
Route::get('projects/switch/{project}', 'ProjectController@switchToDesign');
Route::delete('projects/delete/{project}', 'ProjectController@destroy');

Route::get('designs', 'DesignController@index');
Route::get('designs/{design}','DesignController@edit');
Route::patch('designs/{design}', 'DesignController@update');
Route::delete('/designs/picremove/{photo}', 'PhotoController@destroy');

Route::get('/settings', 'UserController@index');
Route::patch('/settings/{user}', 'UserController@update');
Route::delete('/settings/removeAvatar/{photo}', 'UserController@removeAvatar');
Route::get('/settings/addAdmin', 'UserController@create');
Route::post('/settings/new','UserController@store');

Route::get('/customers', 'CustomerController@index');

Route::get('/messages', 'MessageController@index');
Route::get('/messages/{message}', 'MessageController@show');
Route::get('/messages/edit/{message}', 'MessageController@edit');
Route::delete('messages/delete/{message}', 'MessageController@destroy');

Route::delete('customers/delete/{customer}', 'CustomerController@destroy');
Route::get('/customers/show/{customer}', 'CustomerController@show');

