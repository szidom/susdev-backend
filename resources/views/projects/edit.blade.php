@extends('layouts.projectlayout')

@section('content')

<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading left">Project Controller
                @if(!$project->isNew) 
                    <a href="/projects/switch/{{$project->id}}" class="w3-right">Switch to design <span class="fa fa-exchange fa-2x"></span></a>
                @endif
                </div> 
                <form method="post" name="form" enctype="multipart/form-data" action="{{$project->id}}" class="form">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="panel-body">
                       <div class="form-group">
                            <label for="name">Project title</label>
                            <input type="text" name="name" class="form-control" required value="{{$project->name}}">       
                        </div>
                        <div class="form-group">
                            <label for="description">Description of the project</label>
                            <textarea name="description" rows=10 style="resize:vertical" class="form-control" id="texteditor" required>{{$project->description}}</textarea>
                        </div>
                        @if($project->Photos()->count())
                        <div class="form-group row">
                            <div class="col-md-10">
                            <label>Pictures</label>
                            </div>
                            @foreach($project->Photos()->get() as $photo)
                            <div class="col-md-4">
                            <a href="/{{$photo->location}}">
                            <img class="w3-image" src="/{{$photo->location}}" title="{{$photo->alt}}"  alt="{{$photo->alt}}" />
                            </a>
                            </div>
                            @endforeach
                        </div>
                        @endif
                        @if($project->Location()->count())
                        <div class="form-group">
                            <label for="loc">Location</label>
                            <div class="form-control" name="loc" id="map"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" id="add" name="address" class="form-control" readonly value="{{$project->Location()->first()->address}}">
                        </div>
                        @endif
                        <div class="panel-body row">
                            <div class="col-md-4">
                                <label for="onGoing">Design is in progress</label>
                                @if($project->onGoing)
                                <input type="checkbox" name="onGoing" id="onGoing" onclick="changeCheckState(this)" value=1 checked/>
                                @else
                                <input type="checkbox" name="onGoing" id="onGoing" onclick="changeCheckState(this)" value=0 />
                                @endif
                            </div>
                            <div class="col-md-4">
                                <label for="isReference">Shown in references</label>
                                @if($project->isReference)
                                <input type="checkbox" name="isReference" id="isReference" onclick="changeCheckState(this)" value=1 checked/>
                                @else
                                <input type="checkbox" name="isReference" id="isReference" onclick="changeCheckState(this)" value=0 />
                                @endif
                            </div>
                            <div class="col-md-4">
                                <label for="isImplemented">Implemented</label>
                                <input type="checkbox" name="isImplemented" id="isImplemented" onclick="changeCheckState(this)" value="{{$project->isImplemented}}" {{$project->isImplemented? "checked" : ""}} /> 
                            </div>
                        </div>
                    </div>
                    
                <div class="form-group panel-body">
                    @if($project->isNew)
                    <button name="submitButton" value="accept" type="submit" class="btn btn-primary ">Accept Application</button> 
                    <button name="declineButton" value="decline" type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="#remove_Application">
                    <span class="fa fa-trash"></span>
                    Decline Application</button> 
                    @endif                    
                    <button name="submitButton" value="save" type="submit" class="btn btn-success w3-right">Save changes</button>
                    @if(!empty($project->Customer()->first())) 
                    <p class="col-md-12">Ordered by
                        <a href="/customers/show/{{$project->customer}}">{{$project->Customer()->first()->name}}</a>
                    <p>    
                    @endif
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade" id="remove_Application" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" rofutoversenzle="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Remove {{$project->name}}</h4>
                </div>
                <div class="modal-body">
                    Do you really want to delete the application?
                </div>
                <div class="modal-footer">
                <button type="button" 
                    class="btn btn-default" 
                    data-dismiss="modal"
                    style="float:left;">Cancel</button>
                <form 	method="post" 
                        action="/projects/delete/{{$project->id}}">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <button type="submit" value="delete" class="btn btn-danger">Delete</button>
                </form>
                </div>
            </div>    		
        </div>
    </div>
</div>
<script>
function changeCheckState(element){
        
    if(element.checked){
        element.value=1;
        }
    else
        element.value=0;
}
</script>
<script>
var map;
var marker;
function initMap(){
    var spot = {lat: loc.lat, lng: loc.lng };
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: spot
    });
    
    marker = new google.maps.Marker({
        map: map,   
    });
    marker.setPosition(spot);
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7bZOjzEEMTiymu0GqsZOv9NGgYCH7fr4&callback=initMap">
</script>
@endsection
