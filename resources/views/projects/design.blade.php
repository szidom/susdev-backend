@extends('layouts.projectlayout')

@section('content')

<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading left">{{$project->name}}<a href="/projects/{{$project->id}}" class="w3-right">Switch to project <span class="fa fa-exchange fa-2x"></span></a></div> 
                <form method="post" name="form" enctype="multipart/form-data" action="{{$design->id}}" class="form">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="description">Description of the design</label>
                            <textarea name="description" rows=10 style="resize:vertical" class="form-control" id="texteditor" required>{{empty(old('description'))? $design->description : old('description')}}</textarea>
                        </div>
                        @if($design->Photos()->count())
                        <div class="form-group row">
                            <div class="col-md-10">
                                <label>Pictures</label>
                            </div>
                            @foreach($design->Photos()->get() as $photo)
                            <div class="col-md-4" >
                                <a href="#picremove_{{$photo->id}}" class="w3-right">
                                <span data-toggle="modal"
	                			      data-target="#picremove_{{$photo->id}}"
                                      title="Remove {{$photo->alt}}" class="fa fa-trash">
                                </span>
                                </a>
                                <a href="/{{$photo->location}}">
                                <img class="w3-image" src="/{{$photo->location}}" title="Open {{$photo->alt}}"  alt="{{$photo->alt}}" />
                                </a>
                            </div>
                            @endforeach
                        </div>
                        @endif                        
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="isImplemented">Implemented</label>
                                <input type="checkbox" name="isImplemented" id="isImplemented" onclick="changeCheckState(this)" value="{{$project->isImplemented}}" {{$project->isImplemented? "checked" : ""}} /> 
                                <input type="hidden" id="oldNumOfPhoto" class="form-control" value={{$design->Photos()->count()}}>
                                <input type="hidden" id="newNumOfPhoto" name="newNumOfPhoto" class="form-control" value={{$design->Photos()->count()}}>
                            </div>
                        </div>
                    </div>                    
                <div class="w3-container panel-body"  id="addPhoto">
                    <a href="#"  onclick="addMorePhoto()"><span class="fa fa-plus-circle" aria-hidden="true"></span> Add image</a>
                    <label class="panel-body" >Photo upload (max: 2 Mb)</label>                    
                </div>
                <div class="form-group panel-body">
                    @if($project->onGoing && !$project->isNew)
                    <button name="submitButton" value="finish" type="submit" class="btn btn-primary w3-center">Finish Design</button> 
                    @endif
                    <button name="submitButton" value="save" type="submit" class="btn btn-success w3-right">Save changes</button>      
                </div>
                </form>
                </div>
            </div>
        </div>
    @foreach($design->Photos()->get() as $photo)
    <div class="modal fade" id="picremove_{{$photo->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" rofutoversenzle="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete {{$photo->alt}}</h4>
                </div>
                <div class="modal-body">
                    Do you really want to delete the image?
                </div>
                <div class="modal-footer">
                <button type="button" 
                    class="btn btn-default" 
                    data-dismiss="modal"
                    style="float:left;">Cancel</button>
                <form 	method="post" 
                        action="/designs/picremove/{{$photo->id}}">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <button type="submit" value="delete" class="btn btn-danger">Delete</button>
                </form>
                </div>
            </div>    		
        </div>
    </div>	
    @endforeach
</div>
</div>
<script>
function changeCheckState(element){        
    if(element.checked)
        element.value=1;
    else
        element.value=0;
}
</script>
<script type="text/javascript" src="{{ URL::asset('js/addMorePhoto.js') }}"></script>
@endsection
