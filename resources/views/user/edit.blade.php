@extends('layouts.app')

@section('content')

<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading left">User Settings Page</div> 
        <form method="post" name="userForm" enctype="multipart/form-data" action="/settings/{{$user->id}}" class="form">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
        <div class="row panel-body">
           <div class="col-md-6">
            <div class="form-group">
                <label for="name">User Name</label>
                <input class="form-control" type="text" name="name" required value="{{empty(old('name'))?$user->name : old('name')}}">       
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input class="form-control" type="text" name="email" required value="{{empty(old('email'))? $user->email : old('email')}}">
            </div>
            <div class="form-group">
                <label for="surname">Surname</label>
                <input class="form-control" type="text" name="surname" required value="{{empty(old('surname'))? $user->surname : old('surname')}}">
            </div>
            <div class="form-group">
                <label for="lastname">Lastname</label>
                <input class="form-control" type="text" name="lastname" required value="{{empty(old('lastname'))? $user->lastname : old('lastname')}}">
            </div>
            <div class="form-group">
                <label for="phone">Phone number</label>
                <input class="form-control" type="text" name="phone" value="{{empty(old('phone'))? $user->phone : old('phone')}}">
            </div>
            <div class="form-group">
                <label for="password">Current password</label>
                <input class="form-control" type="password" name="password" required>
            </div>    
            <div class="form-group">
                <label for="new_password">New password</label>
                <input class="form-control" type="password" name="new_password" id="new_password">
            </div>  
            <div class="form-group">
                <label for="new_password_confirmation">Confirm new password</label>
                <input class="form-control" type="password" name="new_password_confirmation" id="new_password_confirmation">
            </div>
                                 
          </div>
          <div class="col-md-6">
            @if($user->Avatar()->count())
                @php
                    $photo = $user->Avatar()->first();
                @endphp
                <div class="form-group col-md-12">
                    <a href="#picremove_{{$photo->id}}" class="w3-right">
                        <span data-toggle="modal"
                            data-target="#picremove_{{$photo->id}}"
                            title="Remove {{$photo->alt}}" class="fa fa-trash">
                        </span>
                    </a>
                    <a href="/{{$photo->location}}">
                    <img id="avatar" class="w3-image w3-right" src="{{$photo->location}}" title="Open" alt="{{$photo->alt}}">
                    </a>      
                </div>
            @endif             
            <div class="form-group">
                <label for="photo" class="col-md-5">
                    @if($user->Avatar()->count())
                        Change avatar
                    @else
                        Upload avatar
                    @endif
                </label>                
                <input class="col-md-7" type="file" accept="image/*" name="photo" id="photo" > 
            </div>      
            <div class="form-group">
                <label class="col-md-12" for="intro">Introduction</label>
                <textarea rows=5 class="form-control" style="resize:vertical" name="intro">{{empty(old('intro'))? $user->intro : old('intro')}}</textarea>
            </div>
              
            <div class="form-group">
                <label class="col-md-8" for="isShown">Shown on the website</label>
                @if($user->isShown)
                <input class="col-md-4" type="checkbox" name="isShown" id="isShown" onclick="changeCheckState(this)" value=1 checked/>
                @else
                <input class="col-md-4" type="checkbox" name="isShown" id="isShown" onclick="changeCheckState(this)" value=0 />
                @endif
            </div>   
              
          </div>
          
        </div>
        <div class="panel-body">
            <button class="btn btn-success w3-right" type="submit" >Update profile</button>
        </div>
    </form>  
                </div>
            </div>
        </div>
    @if($user->Avatar()->count())
    <div class="modal fade" id="picremove_{{$photo->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" rofutoversenzle="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete {{$photo->alt}}</h4>
                </div>
                <div class="modal-body">
                    Do you really want to delete the image?
                </div>
                <div class="modal-footer">
                <button type="button" 
                    class="btn btn-default" 
                    data-dismiss="modal"
                    style="float:left;">Cancel</button>
                <form 	method="post" 
                        action="/settings/removeAvatar/{{$photo->id}}">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <button type="submit" value="delete" class="btn btn-danger">Delete</button>
                </form>
                </div>
            </div>    		
        </div>
    </div>
    @endif
</div>
<script>
function changeCheckState(element){        
    if(element.checked)
        element.value=1;
    else
        element.value=0;
}
</script>
@endsection
