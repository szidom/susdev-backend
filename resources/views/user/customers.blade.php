@extends('layouts.app')

@section('content')

<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading col-md-12">
                        <label class="col-md-2">Username</label>
                        <label class="col-md-2">Surname</label>
                        <label class="col-md-2">Last name</label>
                        <label class="col-md-2">E-mail</label>
                        <label class="col-md-2">Phone</label>
                        <label class="col-md-2">Remove</label>
                </div>
                
                @foreach($customers as $customer)
                <div class="panel-body">
                        <a href="/customers/show/{{$customer->id}}"><p class="col-md-2">{{$customer->name}}</p></a>
                        <p class="col-md-2">{{$customer->surname}}</p>
                        <p class="col-md-2">{{$customer->lastname}}</p>
                        <p class="col-md-2">{{$customer->email}}</p>
                        <p class="col-md-2">{{$customer->phone}}</p>
                        <a href="#userRemove_{{$customer->id}}" class="col-md-1">
                                <span data-toggle="modal"
	                			      data-target="#userRemove_{{$customer->id}}"
                                      title="Remove {{$customer->name}}" class="fa fa-trash">
                                </span>
                        </a>
                </div>
                
                <div class="modal fade" id="userRemove_{{$customer->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" rofutoversenzle="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete {{$customer->name}}</h4>
                            </div>
                            <div class="modal-body">
                                Do you really want to delete the customer?
                            </div>
                            <div class="modal-footer">
                            <button type="button" 
                                class="btn btn-default" 
                                data-dismiss="modal"
                                style="float:left;">Cancel</button>
                            <form 	method="post" 
                                    action="/customers/delete/{{$customer->id}}">
                                {{method_field('DELETE')}}
                                {{csrf_field()}}
                                <button type="submit" value="delete" class="btn btn-danger">Delete</button>
                            </form>
                            </div>
                        </div>    		
                    </div>
                </div>	
                
                @endforeach     
                
                </div>
            </div>
        </div>
    
</div>
@endsection
