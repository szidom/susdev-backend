@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading col-md-12">User data of {{$customer->name}}</div> 
                <div class="panel-body row">
                    @if($customer->surname && $customer->lastname)
                    <p class="w3-text col-md-8" name="name">Name: {{$customer->surname}} {{$customer->lastname}}</p>
                    @endif
                    @if($customer->phone)
                    <p class="w3-text col-md-8" name="phone">Phone: {{$customer->phone}}</p>
                    @endif
                    <p class="w3-text col-md-8" name="mail" >E-mail: {{$customer->email}}</p>   
                    @if(!empty($customer->orderedProjects()->first()))
                    <p class="panel-heading col-md-8">Projects:</p>
                    @foreach($customer->orderedProjects()->get() as $project)
                        <a href="/projects/{{$project->id}}">
                        <p class="col-md-8">{{$project->name}}</p>
                        </a>
                    @endforeach
                    @endif                    
                    @if($customer->Avatar()->count())
                    <div class="container col-md-4">
                        <img class="w3-image w3-right" src="/{{$customer->Avatar()->first()->location}}" 
                        alt="{{$customer->Avatar()->first()->alt}}"/>
                    </div>
                    @endif
                </div>
            </div>
    </div>
</div>

@stop
