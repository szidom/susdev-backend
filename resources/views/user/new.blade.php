@extends('layouts.app')

@section('content')

<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading left">New Admin Registration Page</div> 
        <form method="post" name="userForm" action="new" class="form">
        {{ csrf_field() }}
        <div class="panel-body">
           <div class="row">
            <div class="panel-body">
                <label class="col-md-3" for="name">User Name</label>
                <input class="col-md-9" type="text" name="name" required value="{{old('name')}}">       
            </div>
            <div class="panel-body">
                <label class="col-md-3" for="email">E-mail</label>
                <input class="col-md-9" type="text" name="email" required value="{{old('email')}}">
            </div>    
            <div class="panel-body">
                <label class="col-md-3" for="password">New password</label>
                <input class="col-md-9" type="password" name="password" id="password" required>
            </div>  
            <div class="panel-body">
                <label class="col-md-3" for="password_confirmation">Confirm new password</label>
                <input class="col-md-9" type="password" name="password_confirmation" id="password_confirmation" required>
            </div>
            <div class="panel-body">
                <label for="currentPassword">Please enter your password</label>
                <input class="form-control" type="password" name="currentPassword" id="currentPassword" required>
            </div>                  
          </div>
          
        </div>
        <div class="panel-body">
            <button class="btn btn-success w3-right" type="submit" >Create Admin</button>
        </div>
    </form>  
                </div>
            </div>
        </div>
    
</div>
@endsection
