<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Susdev Design</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
</head>
<body>   
    <div id="wrapper">
        @include ('layouts.nav')    
        
        <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="/home">
                    Susdev Design
                </a>
            </li>
            <label for="sidebar-wrapper">Content</label>
            @foreach($infopages as $infopage)
            <li>
                <a href="/infopages/{{$infopage->id}}">{{$infopage->pageNumber.'. '.$infopage->title}}</a>
            </li>
            @endforeach
        </ul>
    </div>
<!-- /#sidebar-wrapper -->

        <div id="page-content-wrapper"> 
            @yield('content')
            
            @include('layouts.footer')
        </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
    /*function loadTextBody(infoId){
        var i;
        var length = pages.length;
        for(i=0; i<length; i++){
            if(infoId==pages[i].id){
                document.getElementById("selectedItem").value=pages[i].id;
                document.getElementById("texteditor").value=pages[i].content;
                document.getElementById("title").value=pages[i].title;
                document.getElementById("pageNumber").value=pages[i].pageNumber;
                if(page.updated_at){
                    document.getElementById("updatedAt").innerHTML="Last time updated at: ".concat(pages[i].updated_at);
                }else{
                    document.getElementById("updatedAt").innerHTML="";
                }
             }
        }
    }*/
    </script>
</body>
</html>
