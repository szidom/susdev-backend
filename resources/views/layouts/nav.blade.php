<nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    @if (!Auth::guest())
                        <li class="dropdown">
                                <a href="#" class="dropdown-toggle navbar-brand" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li class="{{ setActive('settings') }}">
                                        <a href="/settings">Settings</a>

                                    </li>
                                    <li>
                                        <a href="/settings/addAdmin">Registrate a new admin</a>

                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                    @endif
                    
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if (!Auth::guest())
                            <li class="{{ setActive('infopages') }}">
                                <a href="/infopages">Intro to Permaculture</a>
                            </li>
                            <li class="{{ setActive('projects') }}">
                                <a href="/projects">Projects
                                    @if(session('newProjNot') > 0)
                                    <span class="fa fa-bolt" aria-hidden="true"></span>
                                    @endif
                                </a>
                            </li>
                           @if(!empty(VKAdmin\Design::first()))
                            <li class="{{ setActive('designs') }}">
                                <a href="/designs">Designs</a>
                            </li>
                            @endif
                            <li class="{{ setActive('customers') }}">
                                <a href="/customers">Customers</a>
                            </li>
                            <li class="{{ setActive('messages') }}">
                                <a href="/messages">Messages</a>
                            </li>
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li class="{{ setActive('login') }}">
                                <a href="{{ route('login') }}">Login</a>
                            </li>
                            <!--Remove Register menupoint
                            <li class="{{ setActive('register') }}">
                                <a href="{{ route('register') }}">Register</a>
                            </li>                            
                            -->
                        @else
                            
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        
    
