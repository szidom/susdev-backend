<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Susdev Design</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
</head>
<body>   
    <div id="wrapper">
        @include ('layouts.nav')    
        
        <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="/home">
                    Susdev Design
                </a>
            </li>
            @if(!isset($design))
                @if($project->where('isNew', '=','1')->where('onGoing','=','0')->count() > 0)
                    <label for="sidebar-wrapper">New Assignments</label>
                        @foreach($projects->where('isNew','=','1')->where('onGoing','=','0') as $project)
                        <li>
                            <a href="/projects/{{$project->id}}">{{$project->name}}</a>
                        </li>
                        @endforeach  
                <br>  
                @endif
                @if($project->where('isNew', '=','1')->where('onGoing','=','1')->count() > 0)
                    <label for="sidebar-wrapper">Change Requests</label>
                        @foreach($projects->where('isNew','=','1')->where('onGoing','=','1') as $project)
                        <li>
                            <a href="/projects/{{$project->id}}">{{$project->name}}</a>
                        </li>
                        @endforeach  
                <br>  
                @endif
                @if($project->where('onGoing', '=','1')->where('isNew', '=','0')->count() > 0)
                    <label for="sidebar-wrapper">Ongoing Projects</label>
                    @foreach($projects->where('onGoing','=','1')->where('isNew', '=','0') as $project)
                    <li>
                        <a href="/projects/{{$project->id}}">{{$project->name}}</a>
                    </li>
                    @endforeach 
                <br>   
                @endif 
                @if($project->where('isImplemented', '=','1')->count() > 0)                 
                    <label for="sidebar-wrapper">Implemented Projecs</label>
                    @foreach($projects->where('isImplemented','=','1') as $project)
                    <li>
                        <a href="/projects/{{$project->id}}">{{$project->name}}</a>
                    </li>
                    @endforeach
                <br>
                @endif
                
                <label for="sidebar-wrapper">Other Projecs</label>
                @foreach($projects->where('isImplemented','=','0')->where('onGoing','=','0')->where('isNew','=','0') as $project)
                <li>
                    <a href="/projects/{{$project->id}}">{{$project->name}}</a>
                </li>
                @endforeach            
            @else
                @if($projects->where('onGoing', '=','1')->where('isNew', '=', '1')->count() > 0)
                    <label for="sidebar-wrapper">Change Requests</label>
                    @foreach($projects->where('onGoing','=','1')->where('isNew', '=', '1') as $project)                    
                    <li>
                        <a href="/designs/{{$project->Design()->first()->id}}">{{$project->name}}</a>
                    </li>
                    @endforeach
                <br>  
                @endif 
                @if($projects->where('onGoing', '=','1')->where('isNew', '=', '0')->count() > 0)
                    <label for="sidebar-wrapper">Ongoing Designs</label>
                    @foreach($projects->where('onGoing','=','1')->where('isNew', '=', '0') as $project)                    
                    <li>
                        <a href="/designs/{{$project->Design()->first()->id}}">{{$project->name}}</a>
                    </li>
                    @endforeach
                <br>  
                @endif 
                @if($projects->where('isImplemented', '=','1')->count() > 0)       
                    <label for="sidebar-wrapper">Implemented Designs</label>
                    @foreach($projects->where('isImplemented','=','1') as $project)
                    <li>
                        <a href="/designs/{{$project->Design()->first()->id}}">{{$project->name}}</a>
                    </li>
                    @endforeach
                <br>    
                @endif        
                <label for="sidebar-wrapper">Other Designs</label>
                @foreach($projects->where('isImplemented','=','0')->where('onGoing','=','0')->where('isNew','=','0') as $project)
                <li>
                    <a href="/designs/{{$project->Design()->first()->id}}">{{$project->name}}</a>
                </li>
                @endforeach            
            @endif           
        </ul>
    </div>
<!-- /#sidebar-wrapper -->

        <div id="page-content-wrapper"> 
            @yield('content')
            
            @include('layouts.footer')
        </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
