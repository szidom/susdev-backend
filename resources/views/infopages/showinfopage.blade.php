@extends('layouts.infopageslayout')

@section('content')

<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading left">Introduction to Permaculture Editor<a href="/infopage/create" class="w3-right" ><span class="fa fa-plus fa-2x"></a></div> 
                <form method="post" name="form" enctype="multipart/form-data" action="{{$informationPage->id}}" class="form">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <label for="title" class="w3-left">Title</label>   
                        <label for="pageNumber" class="w3-right">Page Number</label>
                        <hr>
                        <input type="text" name="title" id="title" class="w3-left col-md-7" required 
                            value="{{empty(old('title'))? $informationPage->title : old('title')}}">
                        <select name="pageNumber" id="pageNumber" class="w3-right"> 
                            @foreach($infopages as $ip)
                                @if($ip->id==$informationPage->id)
                                    <option selected="selected" value="{{$informationPage->pageNumber}}">
                                        {{$informationPage->pageNumber}}
                                    </option>
                                @else
                                    <option value="{{$ip->pageNumber}}">{{$ip->pageNumber}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                <div  class="panel-body">
                    <label for="texteditor">Content</label>
                    <textarea rows=10 name="content" style="resize:vertical" class="form-control w3-textarea" id="texteditor" required>{{empty(old('content'))? $informationPage->content : old('content')}}</textarea>
                    @if($informationPage->updated_at)
                    <p class="w3-right" id="updatedAt">Last time updated at: {{$informationPage->updated_at}}</p>
                    @endif
                    <input type="hidden" id="oldNumOfPhoto" class="form-control" value={{$informationPage->Photos()->count()}}>
                    <input type="hidden" id="newNumOfPhoto" name="newNumOfPhoto" class="form-control" value={{$informationPage->Photos()->count()}}>
                </div>
                @if($informationPage->Photos()->count())
                <div class="panel-body">
                    <div class="form-group w3-row">
                        <div class="col-md-10">
                            <label>Pictures</label>
                        </div>
                        @foreach($informationPage->Photos()->get() as $photo)
                        <div class="col-md-4" >
                            <a href="#picremove_{{$photo->id}}" class="w3-right">
                            <span data-toggle="modal"
                                data-target="#picremove_{{$photo->id}}"
                                title="Remove {{$photo->alt}}" class="fa fa-trash">
                            </span>
                            </a>
                            <a href="/{{$photo->location}}">
                            <img class="w3-image-small" src="/{{$photo->location}}" title="Open {{$photo->alt}}"  alt="{{$photo->alt}}" height=142px />
                            </a>
                            <p class="card-text" >Reference number: {{ $photo->number }}</p>
                        </div>                           
                        @endforeach
                    </div>
                </div>
                @endif                     
                
                <div class="w3-container"  id="addPhoto">
                    <label class="panel-body" >Photo upload (max: 2 Mb)</label>                    
                </div>
                
                <div class="form-group panel-body">                    
                    <a href="#" class="w3-left" onclick="addMorePhoto()"><span class="fa fa-plus-circle" aria-hidden="true"></span> Add image</a>
                    <button type="submit" class="btn btn-success w3-right">Save changes</button>      
                </div>
                </form>
            </div>
        </div>
    </div>
@foreach($informationPage->Photos()->get() as $photo)
    <div class="modal fade" id="picremove_{{$photo->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" rofutoversenzle="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete {{$photo->alt}}</h4>
                </div>
                <div class="modal-body">
                    Do you really want to delete the image?
                </div>
                <div class="modal-footer">
                <button type="button" 
                    class="btn btn-default" 
                    data-dismiss="modal"
                    style="float:left;">Cancel</button>
                <form 	method="post" 
                        action="/infopages/picremove/{{$photo->id}}">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <button type="submit" value="delete" class="btn btn-danger">Delete</button>
                </form>
                </div>
            </div>    		
        </div>
    </div>	
@endforeach
</div>
</div>
<script type="text/javascript" src="{{ URL::asset('js/addMorePhoto.js') }}"></script>
@endsection
