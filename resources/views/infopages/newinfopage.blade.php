@extends('layouts.infopageslayout')

@section('content')
<title>New Information Page</title>
<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Introduction to Permaculture Editor</div>
                <form method="post" name="form" enctype="multipart/form-data" action="addnew" class="form">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <label for="title" class="w3-left">Title</label>   
                        <label for="pageNumber" class="w3-right">Page Number</label>
                        <hr>
                        <input type="text" name="title" id="title" class="w3-left col-md-7" required value="{{old('title')}}"> 
                        <select name="pageNumber" id="pageNumber" class="w3-right" readonly>      
                            <option selected="selected" value="{{$infopages->count()}}">{{$infopages->count()}}</option>
                        </select>                 
                    </div>
                <div  class="panel-body">
                    <label for="texteditor">Content</label>
                    <textarea rows=10 name="content" style="resize:vertical" class="form-control w3-textarea" id="texteditor" required>{{old('content')}}</textarea>
                    <input type="hidden" id="newNumOfPhoto" name="newNumOfPhoto" class="form-control" value=0 >
                </div>
                              
                <div class="w3-container"  id="addPhoto">
                    <label class="panel-body" >Photo upload (max: 2 Mb)</label>                    
                </div>
                
                <div class="form-group panel-body">                    
                     <a href="#" class="w3-left" onclick="addMorePhoto()"><span class="fa fa-plus-circle" aria-hidden="true"></span> Add image</a>
                    <button type="submit" class="btn btn-success w3-right">Save new page</button>      
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ URL::asset('js/addPhoto.js') }}"></script>
@endsection
