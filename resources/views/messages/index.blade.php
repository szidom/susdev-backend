@extends('layouts.app')

@section('content')

<div class="w3-container">
    @if ($errors->any())
    <div class="alert alert-danger col-md-8 col-md-offset-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading col-md-12">
                        <label class="col-md-3">E-mail</label>
                        <label class="col-md-3">Subject</label>
                        <label class="col-md-2">New</label>
                        <label class="col-md-3">Date</label>
                        <label class="col-md-1">Remove</label>
                </div>
                
                @foreach($messages as $message)
                <div class="panel-body">
                        <a href="messages/{{$message->id}}"><p class="col-md-3">{{$message->fromTitle}}</p>
                        <p class="col-md-3">{{$message->subject}}</p></a>
                        <p class="col-md-2">
                            @if(!$message->isFromDesigner)
                            <i class="fa fa-free-code-camp" aria-hidden="true"></i>
                            @endif
                        </p>
                        <p class="col-md-3">{{$message->updated_at}}</p>
                        <a href="#messageRemove_{{$message->id}}" class="col-md-1">
                                <span data-toggle="modal"
	                			      data-target="#messageRemove_{{$message->id}}"
                                      title="Remove message" class="fa fa-trash">
                                </span>
                        </a>
                </div>
                
                <div class="modal fade" id="messageRemove_{{$message->id}}" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" rofutoversenzle="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete {{$message->subject}}</h4>
                            </div>
                            <div class="modal-body">
                                Do you really want to delete the message?
                            </div>
                            <div class="modal-footer">
                            <button type="button" 
                                class="btn btn-default" 
                                data-dismiss="modal"
                                style="float:left;">Cancel</button>
                            <form 	method="post" 
                                    action="/messages/delete/{{$message->id}}">
                                {{method_field('DELETE')}}
                                {{csrf_field()}}
                                <button type="submit" value="delete" class="btn btn-danger">Delete</button>
                            </form>
                            </div>
                        </div>    		
                    </div>
                </div>	
                
                @endforeach     
                
                </div>
            </div>
        </div>
    
</div>
@endsection
