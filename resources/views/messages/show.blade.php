@extends('layouts.app')

@section('content')

<div class="w3-container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading left">{{$message->subject}}</div> 
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="message">Message Content</label>
                            <p name="message">{{$message->message}}</p>
                        </div>
                       <div class="form-group">
                            <label for="email">E-mail address</label>
                            <p name="message">{{$message->fromTitle}}</p>
                       </div>
                       @if(!$message->isFromDesigner)
                       <button name="readButton" type="button" class="btn btn-primary " onclick="javascript:location.href='/messages/edit/{{$message->id}}'">I red the message</button>
                       @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function changeCheckState(element){        
    if(element.checked)
        element.value=1;
    else
        element.value=0;
}
</script>
@endsection
