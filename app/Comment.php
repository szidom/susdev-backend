<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    //
    protected $table = 'comments';
    
    protected $fillable = ['content', 'editor', 'mainPost'];
    
    public function Post()
    {
        return $this->belongsTo('VKAdmin\Post', 'mainPost');
    }
}
