<?php

namespace VKAdmin\Http\Controllers;

use VKAdmin\Project;
use VKAdmin\Location;
use VKAdmin\Design;
use File;
use JavaScript;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project=Project::first();
        
        if(empty($project))
            return view('home', ['message' => "Sorry! No existing project yet." ]);
        
        //return view('projects/show',['projects' => $projects, 'project' => $project ]); 
        return redirect()->action('ProjectController@edit', ['id' => $project->id]);    
    }

    /**
     * Display the specified resource.
     *
     * @param  \VKAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \VKAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {   
        $projects=Project::get();
        if($project->Location()->count()){
            $location=$project->Location()->first();
            JavaScript::put([
                'loc' => $location
            ]);
        }
        return view('projects/edit', ['projects' => $projects, 'project' => $project ]);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \VKAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {   
        //dd($request->all());
        switch($request->submitButton){
            case "accept": 
               //dd($request->all());
                
                $design=new Design([
                    'project' => $project->id,
                    'description' => "",
                ]);
                $design->save();
                
                session(['newProjNot' => session('newProjNot')-1 ]);
                
                $project->isNew=false;
                $project->onGoing=true;
                $project->save();
            break;
            
            case "save": 
                $this->validate($request, [
                    'name' => 'bail|required|max:25',
                    'description' => 'required',
                ]);
                $user=Auth::user();
                $project->designer = $user->id;
                
                //One solution to pass checkboxes
                $chks=array('onGoing','isReference','isImplemented');
                foreach($chks as $chk){
                    $project->setAttribute($chk, (Input::has($chk)) ? 1 : 0 );
                }                
                //dd($request->all(), $project, $tmp);
                
                $project->update($request->all());
            break;
        }
        //dd($request->all());
        return back(); 
    }
    
    public function switchToDesign(Project $project){
        if($project->Design()->first()!=NULL){
            return redirect()->action('DesignController@edit', ['id' => $project->Design()->first()->id]);
        }else{
            $design=new Design([
                    'project' => $project->id,
                    'description' => "",
            ]);
            $design->save();
            return redirect()->action('DesignController@edit', ['id' => $design->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \VKAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        /* It doesn't have design in the beginning for sure
        if(!empty($project->Design()->first()){
            $design = $project->Design()->first();
            $design
        }*/
        session(['newProjNot' => session('newProjNot')-1 ]);
        foreach($project->Photos()->get() as $photo){
            File::delete($photo->location);
            $photo->delete();
        }
        
        if(!empty($project->Location()->first())){            
            $location=$project->Location()->first()->delete();
        }
        if(!empty($project->Follower()->first()))
            $project->Follower()->first()->delete();
        if(!empty($project->Design()->first()))
            $project->Design()->first()->delete();
        $project->delete();      
        return redirect()->action('ProjectController@index');
    }
}
