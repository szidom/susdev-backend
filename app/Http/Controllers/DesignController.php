<?php

namespace VKAdmin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use VKAdmin\Photo;
use VKAdmin\Design;
use VKAdmin\Project;
use Image;
use DB;

class DesignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $designId=Design::firstOrFail();
        
        //dd(empty($designId));
        //return view('projects/show',['projects' => $projects, 'project' => $project ]); 
        if(!empty($designId))
            return redirect()->action('DesignController@edit', ['id' => $designId->id]);    
       
    }
    
    public function edit(Design $design)
    {   
        //$designs=Design::paginate(1);
        $designs=Design::get();
        $projects=Project::whereExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('designs')
                      ->whereRaw('designs.project = projects.id');
            })->get();
            
        //$projects=Project::where('Design()->first()->id', ">", '0')->get();
        $project=$design->Project()->first();
        return view('projects/design', ['design' => $design, 'projects' => $projects, 'project' => $project ]);  
    }
    
    public function update(Request $request, Design $design)
    {           
        $valArray=[
            'description' => 'required|min:25', 
        ];
        
        $oldNumOfPhoto = $design->Photos()->count();
        $newNumOfPhoto = $request->input('newNumOfPhoto');          
        if($newNumOfPhoto>$oldNumOfPhoto){              
            for($i=$oldNumOfPhoto;$i<=$newNumOfPhoto;++$i){
                if($request->hasFile('photo'.$i))
                    $valArray['photo'.$i] = 'max:2000';                    
            }
        }
        $this->validate($request, $valArray);
        
        $user=Auth::user();
        $project = $design->Project()->first() ;
        $project->designer=$user->id;
        
        $chks=array('isImplemented');
        foreach($chks as $chk){
            $project->setAttribute($chk, (Input::has($chk)) ? 1 : 0 );
        }  
        
        if($request->submitButton=="finish"){
            $project->onGoing=false;         
        }          
        //dd($request->all(), $project, $tmp);
        $project->save();
        $design->update($request->all());
        
        if($newNumOfPhoto>$oldNumOfPhoto){              
            for($i=$oldNumOfPhoto;$i<=$newNumOfPhoto;++$i){
                if($request->hasFile('photo'.$i)){
                    $img = Image::make($request->file('photo'.$i));
                    $path = 'storage/commonfiles/designs/'.$request->file('photo'.$i)->getClientOriginalName();
                    if($img->filesize()>1500000){
                            $img->fit(1920,1080);
                        }
                    $img->save($path);
                        
                    $photo = new Photo([
                            'alt' => $request->input('alt'.$i),
                            'location' => $path,
                            'number' => $i,
                            'design' => $design->id,
                    ]);  
                    //dd($photo);
                    $photo->save();    
                }       
            }
        }
            
        //dd($request->all());
        return back(); 
    }    
}
