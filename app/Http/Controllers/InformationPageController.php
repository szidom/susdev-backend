<?php

namespace VKAdmin\Http\Controllers;

use VKAdmin\InformationPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
//use JavaScript;
use VKAdmin\Photo;
use Image;

class InformationPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informationPage=InformationPage::orderBy('pageNumber', 'asc')->first();
        
        if(empty($informationPage))
            return redirect()->action('InformationPageController@create');        
        
        return redirect()->action('InformationPageController@edit', ['id' => $informationPage->id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $infopages = InformationPage::orderBy('pageNumber', 'asc')->with('Photos')->get();
              
        return view('infopages.newinfopage', ['infopages' => $infopages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valArray=[
            'title' => 'required|max:25',
            'content' => 'required',
            'pageNumber' => 'min:0',            
        ];
        
        if($request->hasFile('photo1')){
            $newNumOfPhoto = $request->input('newNumOfPhoto');      
            if($newNumOfPhoto>0){              
                for($i=1;$i<=$newNumOfPhoto;++$i){
                    $valArray['photo'.$i] = 'max:2000';                    
                }
            }
        }
        $this->validate($request, $valArray);
        
        $user=Auth::user();
        
        $infoPage=new InformationPage($request->all());
        
        $infoPage->editor = $user->id;
        $infoPage->save();
        
        if($request->hasFile('photo1')){
            
            $newNumOfPhoto = $request->input('newNumOfPhoto');      
            if($newNumOfPhoto>0){                 
                for($i=1; $i<=$newNumOfPhoto; ++$i){
                    if($request->hasFile('photo'.$i)){
                        $img = Image::make($request->file('photo'.$i));
                        $path = 'storage/commonfiles/permaculture/'.$request->file('photo'.$i)->getClientOriginalName();
                        if($img->filesize()>1000000){
                            $img->fit(1366,768);
                        }
                        $img->save($path);
                        
                        $photo = new Photo([
                            'alt' => $request->input('alt'.$i),
                            'location' => $path,
                            'informationPage' => $infoPage->id,
                            'number' => $i,
                        ]);  
                        $photo->save();    
                    }       
                }
            }
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \VKAdmin\InformationPage  $informationPage
     * @return \Illuminate\Http\Response
     */
    public function show(InformationPage $informationPage)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \VKAdmin\InformationPage  $informationPage
     * @return \Illuminate\Http\Response
     */
    public function edit(InformationPage $informationPage)
    {
        $infopages = InformationPage::orderBy('pageNumber', 'asc')->with('Photos')->get();
        
        /*JavaScript::put([
            //'pages' => $infopages,
            'page' => $informationPage
        ]);*/
        
        return view('infopages/showinfopage',['infopages' => $infopages, 'informationPage' => $informationPage ]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \VKAdmin\InformationPage  $informationPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InformationPage $informationPage)
    {
        $valArray=[
            'title' => 'required|max:50',
            'content' => 'required',
            'pageNumber' => 'min:0',            
        ];
        
        $oldNumOfPhoto= empty($informationPage->Photos()) ? 0 : $informationPage->Photos()->count() ;
        //dd($request->hasFile('photo'.($oldNumOfPhoto+1)), $request->all(), $oldNumOfPhoto);
        if($request->hasFile('photo'.($oldNumOfPhoto+1))){
            $newNumOfPhoto = $request->input('newNumOfPhoto');              
            for($i=$oldNumOfPhoto+1;$i<=$newNumOfPhoto;++$i){
                $valArray['photo'.$i] = 'max:2000';                    
            }
        }
        $this->validate($request, $valArray);
            
        
        $user=Auth::user();
        
        //changing the value
        $newInfoPage = new InformationPage($request->all());
        if($newInfoPage->pageNumber!=$informationPage->pageNumber){
            $tmpInfoPage=InformationPage::where('pageNumber', '=', $newInfoPage->pageNumber)->first();
            $tmpInfoPage->pageNumber=$informationPage->pageNumber;
            $tmpInfoPage->save();            
        }
        
        $informationPage->editor = $user->id;
        $informationPage->update($request->all());
        
        if($request->hasFile('photo'.($oldNumOfPhoto+1))){
            
            $newNumOfPhoto = $request->input('newNumOfPhoto');      
            if($newNumOfPhoto>$oldNumOfPhoto){         
                 
                for($i=$oldNumOfPhoto+1; $i<=$newNumOfPhoto; ++$i){
                    if($request->hasFile('photo'.$i)){
                        $img = Image::make($request->file('photo'.$i));
                        $path = 'storage/commonfiles/permaculture/'.$request->file('photo'.$i)->getClientOriginalName();
                        if($img->filesize()>1000000){
                            $img->fit(1366,768);
                        }
                        $img->save($path);
                        
                        $photo = new Photo([
                            'alt' => $request->input('alt'.$i),
                            'location' => $path,
                            'informationPage' => $informationPage->id,
                            'number' => $i,
                        ]);  
                        $photo->save();          
                    }
                }
            }
        }
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \VKAdmin\InformationPage  $informationPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(InformationPage $informationPage)
    {
        //
    }
}
