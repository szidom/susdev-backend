<?php

namespace VKAdmin\Http\Controllers;

use Illuminate\Http\Request;
use VKAdmin\Project;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projNum=Project::where('isNew', '=', '1')->get()->count();
        session(['newProjNot' => $projNum ]);
        
        return view('home');
    }
}
