<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    //
    use SoftDeletes;
    
    protected $table = 'faqs';
    
    protected $fillable = ['question', 'answer', 'addedBy'];    
}
