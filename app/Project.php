<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Project extends Model
{
    use SoftDeletes;
    use Notifiable;
    
    protected $table = 'projects';

    protected $fillable = ['name', 'customer', 'designer', 'located', 'description', 'isReference', 'isImplemented', 'onGoing','isNew'];
    

    
    public function Customer()
    {
        return $this->belongsTo('VKAdmin\Customer', 'customer');
    }
    
    public function Designer()
    {
        return $this->belongsTo('VKAdmin\User', 'designer');
    }
    
    public function Location()
    {
        return $this->belongsTo('VKAdmin\Location', 'located');
    }
    
    public function Photos()
    {
        return $this->hasMany('VKAdmin\Photo', 'project');
    }
    
    public function Follower()
    {
        return $this->hasOne('VKAdmin\Follower', 'project_id');
    }    
    
    public function Design()
    {
        return $this->hasOne('VKAdmin\Design', 'project');
    }
    
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
