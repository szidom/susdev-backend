<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InformationPage extends Model
{
    use SoftDeletes;    
    
    protected $table = 'information_pages';
    
    protected $fillable = ['title', 'content', 'references', 'editor','pageNumber',];   
    
    public function Editor()
    {    
        return $this->hasOne('VKAdmin\User', 'editor');
    }
    
    public function Photos()
    {
        return $this->hasMany('VKAdmin\Photo', 'informationPage');
    } 
}
