<?php

namespace VKAdmin;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'designers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'intro', 'isShown', 'phone', 'avatar', 'surname', 'lastname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function madeProjects()
    {
        return $this->hasMany('VKAdmin\Project', 'designer');
    }
    
    public function Avatar()
    {
        return $this->belongsTo('VKAdmin\Photo', 'avatar');
    }
    
}
