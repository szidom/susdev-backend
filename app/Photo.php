<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use SoftDeletes;
    //    
    protected $table = 'photos';
    
    protected $fillable=['alt', 'location', 'number', 'project', 'informationPage', 'design'];
    
    public function Customer()
    {
        return $this->hasOne('VKAdmin\Customer', 'avatar');    
    }
    
    public function Designer()
    {
        return $this->hasOne('VKAdmin\User', 'avatar');
    }
    
    public function Project()
    {
        return $this->belongsTo('VKAdmin\Project', 'project');
    }
    
    public function InformationPage()
    {
        return $this->belongsTo('VKAdmin\InformationPage',  'informationPage');
    }
    
    public function Design()
    {
        return $this->belongsTo('VKAdmin\Design', 'design');
    }
}
