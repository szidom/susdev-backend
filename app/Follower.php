<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Follower extends Model
{
    use SoftDeletes;
    
    protected $table = 'followers';
    
    protected $fillable = ['number', 'project_id',];
    
    public function Project()
    {
        return $this->belongsTo('VKAdmin\Project', 'project_id');
    }
    
    public function Customers()
    {
        return $this->belongsToMany('VKAdmin\Customer')->withTimestamps();
    }
}
