<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    //
    
    protected $table = 'users';
    
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'phone', 'surname', 'lastname'
    ];
    
    public function orderedProjects()
    {
        return $this->hasMany('VKAdmin\Project', 'customer');
    }
    
    public function Avatar()
    {
        return $this->belongsTo('VKAdmin\Photo', 'avatar');
    }
    
    public function followedProjects()
    {
        return $this->belongsToMany('VKAdmin\Follower')->withTimestamps();
    }
    
}
