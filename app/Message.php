<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Message extends Model
{
    use Notifiable;
    use SoftDeletes;    
    //
    protected $table = 'messages';    
    
    protected $fillable = ['toTitle','subject','message', 'fromTitle', 'isFromDesigner'];
    //In frontend to is always a Designer, and from is always a User
    /*
    public function Sender(){
        if($this->isFromDesigner)
            return $this->hasOne('VKAdmin\User', 'from');
        else
            return $this->hasOne('VKAdmin\Customer', 'from');
    }
    
    public function Reciever(){
        if($this->isFromDesigner)
            return $this->hasOne('VKAdmin\Customer', 'to');
        else
            return $this->hasOne('VKAdmin\User', 'to');
    }*/
}
