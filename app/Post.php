<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    //
    protected $table = 'posts';
    
    protected $fillable = ['content', 'editor'];
    
    public function Editor()
    {
        return this->belongsTo('VKAdmin\Customer', 'editor');
    }
    
    public function Comments()
    {
        return $this->hasMany('VKAdmin\Comment','mainPost');
    }
}
