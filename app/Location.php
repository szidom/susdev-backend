<?php

namespace VKAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    //
    
    protected $table = 'locations';
    
    protected $fillable = ['name', 'address','lat','lng']; 
    
    public function Project()
    {
        return $this->hasOne('VKAdmin\Project', 'located');
    }  
}
