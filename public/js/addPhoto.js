function addMorePhoto(){
    var newNumOfPhoto = document.getElementById('newNumOfPhoto').value;
    if( newNumOfPhoto>0 && document.getElementById("photo".concat(newNumOfPhoto)).files.length==0){
        alert("The previously added file was not uploaded yet, so you can not add more files, before you upload that.");
    }
    else{
        newNumOfPhoto++;
        document.getElementById('newNumOfPhoto').value = newNumOfPhoto;
        var addPhotoDiv=document.createElement('div');
        addPhotoDiv.className="panel-body";
        addPhotoDiv.innerHTML="<label class=\"w3-left\" for=\"photo".concat(newNumOfPhoto) + "\">Image upload: </label>" +
        "<input class=\"w3-right\" type=\"text\" name=\"alt".concat(newNumOfPhoto)   +"\" id=\"alt".concat(newNumOfPhoto) + "\" required>"+
        "<label class=\"w3-right\" for=\"alt".concat(newNumOfPhoto) +"\">Image name:  </label>" +
        "<input class=\"w3-left\" type=\"file\" accept=\"image/*\" name=\"photo".concat(newNumOfPhoto) +"\" id=\"photo".concat(newNumOfPhoto) +"\">" ;
        document.getElementById('addPhoto').appendChild(addPhotoDiv);  
    }
}
