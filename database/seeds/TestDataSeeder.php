<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('designers')->insert([
           'name' => 'Szuper Admin',
           'email' => 'admin@admin.hu',
           'password' => bcrypt('admin'),
           'intro' => 'Ez egy bemutatkozo szoveg. Erted?',
           'phone' => '+36703026316',
        ]);
    }
}
